wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod 777 Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh -b
mkdir /home/ubuntu/workspace/.c9/runners
touch /home/ubuntu/workspace/.c9/runners/PythonMiniconda3.run
cat > /home/ubuntu/workspace/.c9/runners/PythonMiniconda3.run  << "EOF"
// This file overrides the built-in Python 3 runner
// For more information see http://docs.c9.io:8080/#!/api/run-method-run
{
  "cmd": [
    "/home/ubuntu/miniconda3/bin/python3",
    "$file",
    "$args"
  ],
  "selector": "^.*\\.(python|py)$",
  "python_version": "python3",
  "info": "Python Miniconda3. Your code is running at \\033[01;34m$url\\033[00m.\n\\033[01;31mImportant:\\033[00m use \\033[01;32mos.getenv('PORT', 8080)\\033[00m as the port and \\033[01;32mos.getenv('IP', '0.0.0.0')\\033[00m as the host in your scripts!\n",
  "env": {
    "PYTHONPATH": "/home/ubuntu/miniconda3/lib/python36.zip:/home/ubuntu/miniconda3/lib/python3.6:/home/ubuntu/miniconda3/lib/python3.6/lib-dynload:/home/ubuntu/miniconda3/lib/python3.6/site-packages:/home/ubuntu/miniconda3/lib/python3.6/site-packages/setuptools-27.2.0-py3.6.egg"
  }
}
EOF
cat > ~/.bashrc << "EOF"
#Added by Miniconda3 automatic setup script
export PATH="$HOME/miniconda3/bin:$PATH"
EOF